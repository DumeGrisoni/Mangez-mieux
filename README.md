# Mangez-mieux

## Evaluation STUDI 01 Site d'association (vitrine) Mobile First, utilisant HTML/CSS et Boostrap 4.

### Mise en situation :
Association dedié aux personnes rencontrant des difficultés lors de la préparation de repas sain.
Nous les aidons à créer des menus rapide et équilibré, le tout à faible coup.

Chez Mangez-mieux nous mettons en avant le gout, alors n'ayez craintes... sain n'est pas forcement égal a fade !
